from glob import glob
import numpy.typing as npt
import numpy as np
from dataclasses import dataclass
import matplotlib.pyplot as plt

fnames = glob("*/calcs.txt")

@dataclass
class Result(object):
    job_id: str
    basis: str
    e_mean: float
    energies: npt.NDArray
    cost: float

data = []

for fname in fnames:
    lines = open(fname).readlines()
    for line in lines:
        job_id = fname.split("/")[0]

        parts = line.split()
        basis = parts[0]
        
        parts[1:] = [float(part) for part in parts[1:]]
        energies = np.array(parts[1:])
        e_mean = np.mean(energies)

        cost = 0
        try:
            basis_lines = open(f"{job_id}/{basis}").readlines()
            for basis_line in basis_lines:
                if "- Basis Cost:" in basis_line:
                    cost += float(basis_line.split()[3])
        except:
            pass

        data.append(Result(job_id, basis, e_mean, energies, cost))

data.sort(key=lambda x: -x.e_mean)

for datum in data:
    print(datum)

costs = [datum.cost for datum in data]
e_means = [datum.e_mean for datum in data]

plt.scatter(costs, e_means)
plt.show()
