#!/usr/bin/env python
from ase.io import read
from gpaw import GPAW, MarzariVanderbilt, setup_paths
from sys import argv
import numpy as np
from gpaw.atom.configurations import parameters
from gpaw.atom.generator import Generator
from tgmbasis.basis import (
    make_soft_confined,
    make_QG_polarization,
    write_basis,
    collect_basis_functions,
)
from random import choice, randint, uniform
from ase.parallel import rank


setups = ["/home/tgmaxson/tgm-basis/optimizer/"]

generator = Generator("Ag", scalarrel=True, xcname="PBE", txt=None, nofiles=True)
generator.N *= 8
generator.run(write_xml=True, name=None, **parameters[generator.symbol])

f = open("calcs.txt", "w") if rank == 0 else None

if True:
    for index in range(50):
        try:
            amp = 12

            s_cut = 8.5
            s_conf = 6.5
            s_splits = [
                uniform(2, s_conf * 0.7),
                uniform(4, s_conf * 0.85),
                uniform(6, s_conf),
            ]

            p_char = uniform(1.6, 3)
            p_cut = p_char * 4
            p_splits = [uniform(2, p_char * 2), uniform(3, p_char * 2)]

            d_cut = 7.5
            d_conf = 5.5
            d_splits = [uniform(4, d_conf)]

            f_char = uniform(0.9, 1.5)
            f_cut = p_char * 4

            basis_functions, description = collect_basis_functions(
                make_soft_confined(generator, "5s", s_cut, s_conf, amp, s_splits),
                make_QG_polarization(p_cut, p_char, 1, p_splits),
                make_soft_confined(generator, "4d", d_cut, d_conf, amp, d_splits),
                make_QG_polarization(f_cut, f_char, 3, []),
            )

            write_basis("Ag", basis_functions, description, f"test-{index}")

            total_energy = []

            images = read(f"../configs.traj", "3:4")

            for atoms in images:
                calc = GPAW(
                    mode="lcao",
                    basis=f"test-{index}",
                    xc="PBE",
                    h=0.14,
                    parallel={
                        "augment_grids": True,
                        "sl_auto": True,
                        "use_elpa": True,
                        "band": 1,
                    },
                    kpts=(1, 1, 1),
                    occupations=MarzariVanderbilt(0.05),
                    symmetry={"point_group": False},
                    txt="-",
                    convergence={
                        "energy": 1e-6,
                        "density": 1e-6,
                        "minimum iterations": 10,
                    },
                    maxiter=100,
                )

                atoms.calc = calc
                total_energy.append(atoms.get_potential_energy() / len(atoms))
            
            if rank == 0:
                print(f"Ag.test-{index}.basis", *total_energy, file=f)
                f.flush()
        except:
            pass
