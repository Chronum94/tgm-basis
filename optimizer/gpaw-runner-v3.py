#!/usr/bin/env python
from ase.io import read
from gpaw import GPAW, MarzariVanderbilt, setup_paths
from sys import argv
import numpy as np
from glob import glob
from ase.parallel import rank


f = open("calcs.txt", "w") if rank == 0 else None

if True:
    for basis in glob("/home/tgmaxson/szilvasi-basis/general-v3/Ag.*"):
        basis = basis.split("/")[-1].split(".")[1]
        try:
            total_energy = []

            images = read(f"../configs.traj", "2:-1")

            for atoms in images:
                calc = GPAW(
                    mode="lcao",
                    basis=f"{basis}",
                    xc="PBE",
                    h=0.14,
                    parallel={
                        "augment_grids": True,
                        "sl_auto": True,
                        "use_elpa": True,
                        "band": 1,
                    },
                    kpts=(1, 1, 1),
                    occupations=MarzariVanderbilt(0.05),
                    symmetry={"point_group": False},
                    txt="-",
                    convergence={
                        "energy": 1e-6,
                        "density": 1e-6,
                        "minimum iterations": 10,
                    },
                    maxiter=100,
                )

                atoms.calc = calc
                total_energy.append(atoms.get_potential_energy() / len(atoms))
            
            if rank == 0:
                print(f"v3/Ag.{basis}.basis", *total_energy, file=f)
                f.flush()
        except Exception as e:
            print(e)
            pass
