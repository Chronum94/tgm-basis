from ase.calculators.emt import EMT
from ase.io import read, write
from ase.md.langevin import Langevin
from ase.units import fs
from mace.calculators import mace_mp

atoms = read("POSCAR")
atoms.calc = mace_mp()

configs = []

while(len(atoms) > 1):
    dyn = Langevin(atoms, 0.5 * fs, temperature_K=50, friction=0.02, logfile="-")
    dyn.run(500)

    dyn = Langevin(atoms, 0.5 * fs, temperature_K=250, friction=0.02, logfile="-")
    dyn.run(200)
    
    configs.append(atoms.copy())
    del atoms[[0]]

configs.append(atoms.copy())

write("configs.traj", configs)
