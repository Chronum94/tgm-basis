from gpaw.basis_data import BasisFunction, Basis, BasisPlotter
from gpaw.atom.generator import Generator
from tgmbasis.confinement import SoftConfinementPotential
from tgmbasis.phit import (phit_confined, 
                           phit_polarization_quasigauss, 
                           phit_split_valence, 
                           phit_reduce)
from tgmbasis.utilities import basis_letters, basis_information, multi_letters
from gpaw.atom.radialgd import EquidistantRadialGridDescriptor
import numpy as np
from gpaw.utilities import divrl
from gpaw import __version__ as version


DEFAULT_GRID_SPACING = 1/64
MAX_BASIS_SIZE = 50 # Bohr
PHIT_ZERO_THRESHOLD = 0 #1e-12


def make_soft_confined(generator: Generator, orbital_label: str, r_cut: float, r_conf_start: float, conf_amplitude: float, 
                       r_splits: list[float], grid_spacing: float=DEFAULT_GRID_SPACING) -> tuple[list[BasisFunction], str]:
    # Extract this information, we need to describe basis function
    n = int(orbital_label[0])
    l = basis_letters.index(orbital_label[1])
    
    # Hold our basis functions and description of what has been generated
    basis_functions = []
    description = ""

    # Make grid to evaluate phit on
    r = np.arange(0, r_cut + grid_spacing, grid_spacing)

    # Make confined orbital
    confinement = SoftConfinementPotential(r_cut, r_conf_start, conf_amplitude)
    phit = phit_confined(generator, orbital_label, confinement, r)
    phit = phit_reduce(phit, min_val=PHIT_ZERO_THRESHOLD)

    # Add basis function and description
    basis_functions.append(BasisFunction(n, l, r[len(phit) - 1], phit, type=f"{orbital_label}-SOFT"))
    description += f"Soft confined {orbital_label}\n"
    description += f" - Cutoff Radius: {r_cut:.3f}\n"
    description += f" - Confinement Start Radius: {r_conf_start:.3f}\n"
    description += f" - Confinement Amplitude: {conf_amplitude:.1f}\n"
    description += basis_information(r_cut, l)
    description += "\n"

    # Now make split valence
    basis_functions_split, description_split = _make_split_valence(phit, r_splits, orbital_label, r)
    basis_functions.extend(basis_functions_split)
    description += description_split

    return basis_functions, description


def make_QG_polarization(r_cut: float, r_char: float, l: int, 
                      r_splits: list[float], grid_spacing: float=DEFAULT_GRID_SPACING) -> tuple[list[BasisFunction], str]:
    
    # Make grid to evaluate phit on
    r = np.arange(0, r_cut + grid_spacing, grid_spacing)
    phit = phit_polarization_quasigauss(r_char, r_cut, l, r)
    phit = phit_reduce(phit, min_val=PHIT_ZERO_THRESHOLD)
    
    # Hold our basis functions and description of what has been generated
    basis_functions = []
    description = ""

    # Add basis function and description
    basis_functions.append(BasisFunction(None, l, r[len(phit) - 1], phit, type=f"{basis_letters[l]}-POL [QG]"))
    description += f"{basis_letters[l]}-polarization [QG]\n"
    description += f" - Cutoff Radius: {r_cut:.3f}\n"
    description += f" - Characteristic Radius: {r_char:.3f}\n"
    description += f" - Angular Number: {l}\n"
    description += basis_information(r_cut, l)
    description += "\n"

    # Now make split valence
    basis_functions_split, description_split = _make_split_valence(phit, r_splits, f"{basis_letters[l]}-POL", r, l=l, suffix=" [QG]")
    basis_functions.extend(basis_functions_split)
    description += description_split

    return basis_functions, description

# Internal function make split valence, users can use manually if needed
def _make_split_valence(phit, r_splits, orbital_label, r, l=None, suffix="") -> tuple[list[BasisFunction], str]:
    try:
        n = int(orbital_label[0])
    except ValueError:
        n = None

    if l is None:
        l = basis_letters.index(orbital_label[1])

    basis_functions = []
    description = ""

    for index, r_split in enumerate(r_splits):
        phit_split = phit_split_valence(phit, r, r_split, l)
        phit = phit_reduce(phit, min_val=PHIT_ZERO_THRESHOLD)
        print(phit[:20])

        basis_functions.append(BasisFunction(None, l, r[len(phit_split) - 1], phit_split, type=f"{orbital_label}-{multi_letters[index + 2]}Z {suffix}"))
        description += f"Split Valence ({multi_letters[index + 2]}Z)\n"
        description += f" - Parent Orbital: {orbital_label}{suffix}\n"
        description += f" - Cutoff Radius: {r_split:.3f}\n"
        description += basis_information(r_split, l)
        description += "\n"

    return basis_functions, description


def collect_basis_functions(*args):
    all_basis_functions = []
    descriptions = []

    for basis_functions, description in args:
        all_basis_functions.extend(basis_functions)
        descriptions.append(description)

    description = "--------------\n\n".join(descriptions)
    
    return all_basis_functions, description


def make_basis(symbol, basis_functions: list[BasisFunction], description, label, grid_spacing: float=DEFAULT_GRID_SPACING):
    rgd = EquidistantRadialGridDescriptor(grid_spacing, int(MAX_BASIS_SIZE / grid_spacing))

    for bf in basis_functions:
        # We have been storing phit_g * r, but we just want phit_g
        r = rgd.r_g[:len(bf.phit_g)]
        bf.phit_g = divrl(bf.phit_g, 1, r)
        bf.phit_g *= r**bf.l
        bf.phit_g[-1] = 0.

    basis = Basis(symbol, label, False, rgd)
    basis.bf_j = basis_functions
    basis.generatordata = description.strip()
    basis.generatorattrs = {'version': version}
    return basis


def write_basis(symbol, basis_functions: list[BasisFunction], description, label, filename=None, grid_spacing: float=DEFAULT_GRID_SPACING):
    basis = make_basis(symbol, basis_functions, description, label, grid_spacing)

    if filename is None:
        basis.write_xml()
    else:
        with open(filename, 'w') as fd:
            basis.write_to(fd)


def plot_basis(basis: Basis, premultiply=False):
    bp = BasisPlotter(premultiply=premultiply, show=True)
    bp.plot(basis)

