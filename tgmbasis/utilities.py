from math import floor
import numpy as np
import numpy.typing as npt
from matplotlib.axes import Axes
from gpaw.basis_data import Basis
from gpaw.atom.generator import Generator
from ase.units import Hartree
import typing
if typing.TYPE_CHECKING:
    from tgmbasis.confinement import ConfinementPotential


multi_letters = '_SDTQ56789'
basis_letters = 'spdfgh'

def get_basis_phit(element, basis, filename, index=0):
    basis = Basis(element, basis, False)
    basis.read_xml(filename)
    phit = basis.bf_j[index].phit_g
    r = basis.rgd.r_g[:len(phit)]
    return phit, r


def plot_phit(ax: Axes, r: npt.NDArray, phit: npt.NDArray, r_factor=True, normalize=False, *args, **kwargs):
    """
    Plots phit of a basis function given phit(r) and r.  Accepts
    additional args/kwargs for plotting.

    Args:
        ax (Axes): Matplotlib subplot to plot on
        r (npt.NDArray): radial gridpoints
        phit (npt.NDArray): phitg at corresponding radial gridpoints
        r_factor (bool): Controls if the phit is multiplied by r
    """
    norm = ((r[1] - r[0]) * phit * phit).sum() ** .5 if normalize else 1
    phit = phit.copy()

    if r_factor:
        phit *= r[:len(phit)]

    if normalize:
        norm = ((r[1] - r[0]) * phit * phit).sum() ** .5 
        phit /= norm

    ax.plot(r[:len(phit)], phit, *args, **kwargs)


def derivative(f: npt.NDArray, x: npt.NDArray) -> npt.NDArray: 
    """
    Calculate the 2nd order central finite difference of f
    with respect to x, given f(x) and x for some sampled points.

    The end-points are treated as second order forwards and backwards
    finite differences.

    Args:
        f (npt.NDArray): Values of f(x)
        x (npt.NDArray): Values of x

    Returns:
        npt.NDArray: Values of df/dx for corresponding values of x
    """
    df_dx = np.zeros(len(f))

    # Second-order forward difference for the first point
    df_dx[0] = (-3*f[0] + 4*f[1] - f[2]) / (2*(x[1] - x[0]))
    
    # Central difference for the interior points
    df_dx[1:-1] = (f[2:] - f[:-2]) / (x[2:] - x[:-2])
    
    # Second-order backward difference for the last point
    df_dx[-1] = (3*f[-1] - 4*f[-2] + f[-3]) / (2*(x[-1] - x[-2]))

    return df_dx


def smoothify(generator, psi_mg, l):
    r"""Generate pseudo wave functions from all-electron ones.

    The pseudo wave function is::

                                ___
            ~                   \    /   ~             \    ~     ~
        | psi  > = | psi  > +   )  | | phi > - | phi > ) < p  | psi > ,
                m          m      /__  \     i         i /     i      m
                                i

    where the scalar products are found by solving::

                        ___
            ~             \     ~             ~     ~
        < p | psi  > =   )  < p  | phi  > < p  | psi  > .
            i     m      /__    i      j      j      m
                            j

    In order to ensure smoothness close to the core, the
    all-electron wave function and partial wave are then
    multiplied by a radial function which approaches 0 near the
    core, such that the pseudo wave function approaches::

                    ___
            ~        \      ~       ~     ~
        | psi  > =   )  | phi >  < p  | psi >    (for r << rcut),
                m      /__      i      i      m
                        i

    which is exact if the projectors/pseudo partial waves are complete.
    """
    g = generator

    if psi_mg.ndim == 1:
        return smoothify(g, psi_mg[None], l)[0]

    u_ng = g.u_ln[l]
    q_ng = g.q_ln[l]
    s_ng = g.s_ln[l]

    Pi_nn = np.dot(g.r * q_ng, u_ng.T)
    Q_nm = np.dot(g.r * q_ng, psi_mg.T)
    Qt_nm = np.linalg.solve(Pi_nn, Q_nm)

    # Weight-function for truncating all-electron parts smoothly near core
    gmerge = g.r2g(g.rcut_l[l])
    w_g = np.ones(g.r.shape)
    w_g[0:gmerge] = (g.r[0:gmerge] / g.r[gmerge])**2.
    w_g = w_g[None]

    psit_mg = psi_mg * w_g + np.dot(Qt_nm.T, s_ng - u_ng * w_g)
    return psit_mg


def resolve_grid(radius, spacing):
    return floor(radius / spacing)


def rcut_by_energy(generator: Generator, j:int,  esplit: float=.1, rguess: float=6., confinement: 'ConfinementPotential'=None):
    """Find confinement cutoff corresponding to given orbital energy shift.

    Creates a confinement potential for the orbital given by j,
    such that the confined-orbital energy is (emin to emax) eV larger
    than the free-orbital energy."""
    g = self.generator
    e_base = g.e_j[j]
    rc = rguess

    if confinement is None:
        confinement_potential = None
    else:
        confinement_potential = confinement.construct_potential(r)

    psi_g, e = g.solve_confined(j, rc, vconf)
    de_min, de_max = esplit / Hartree, (esplit + tolerance) / Hartree

    rmin = 0.
    rmax = g.r[-1]

    de = e - e_base
    # print '--------'
    # print 'Start bisection'
    # print'e_base =',e_base
    # print 'e =',e
    # print '--------'
    while de < de_min or de > de_max:
        if de < de_min:  # Move rc left -> smaller cutoff, higher energy
            rmax = rc
            rc = (rc + rmin) / 2.
        else:  # Move rc right
            rmin = rc
            rc = (rc + rmax) / 2.
        if vconf is not None:
            vconf = g.get_confinement_potential(amplitude, ri_rel * rc, rc)
        psi_g, e = g.solve_confined(j, rc, vconf)
        de = e - e_base
        # print 'rc = %.03f :: e = %.03f :: de = %.03f' % (rc, e*Hartree,
        #                                                  de*Hartree)
        # if rmin - rmax < 1e-
        if g.r2g(rmax) - g.r2g(rmin) <= 1:  # adjacent points
            break  # cannot meet tolerance due to grid resolution
    # print 'Done!'
    return psi_g, e, de, vconf, rc


def basis_information(radius: float, l: int) -> str:
    description = ""
    description += f" - Basis Functions: {(l*2) + 1}\n"
    description += f" - Basis Volume: {(4/3) * np.pi * (radius**3):.3f}\n"
    description += f" - Basis Cost: {((4/3) * np.pi * (radius**3)) * ((l*2) + 1):.3f}\n"
    return description