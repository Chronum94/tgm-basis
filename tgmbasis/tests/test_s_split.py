from gpaw.atom.configurations import parameters
from gpaw.atom.generator import Generator
from tgmbasis.basis import make_soft_confined, make_QG_polarization, write_basis, collect_basis_functions, make_basis, plot_basis

generator = Generator("Ag", scalarrel=True, xcname="PBE", txt=None, nofiles=True)
generator.N *= 4
generator.run(write_xml=True, name=None, **parameters[generator.symbol])

basis_functions, description = collect_basis_functions(
 #make_soft_confined(generator, "5s", 8.5, 6.5, 12, [6.391, 4.877, 2.144]),
 #make_QG_polarization(7, 2.066, 1, [3.457, 2.757]),
 #make_soft_confined(generator, "4d", 7.5, 5.5, 12, [4.223]),
 make_QG_polarization(1.148 * 4 , 1.148, 3, []),
)

#basis = make_basis("Ag", basis_functions, description, "test-fix")
#plot_basis(basis, premultiply=True)
write_basis("Ag", basis_functions, description, "test-fix")
