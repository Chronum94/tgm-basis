from gpaw.atom.configurations import parameters
from gpaw.atom.generator import Generator
from tgmbasis.basis import make_soft_confined, make_QG_polarization, write_basis, collect_basis_functions

generator = Generator("Ag", scalarrel=True, xcname="PBE", txt=None, nofiles=True)
generator.N *= 4
generator.run(write_xml=True, name=None, **parameters[generator.symbol])

basis_functions, description = collect_basis_functions(
 make_soft_confined(generator, "5s", 7.5, 6.5, 12, [5.5, 4.5, 3.5]),
 make_QG_polarization(6.5, 3.2, 1, [3.5, 2.4]),
 make_soft_confined(generator, "4d", 5.5, 3.5, 12, [3.0]),
 make_QG_polarization(4.5, 2.1, 3, []),
)

write_basis("Ag", basis_functions, description, "test")
