from tgmbasis.utilities import plot_phit, basis_letters, smoothify
import matplotlib.pyplot as plt 
import numpy as np
from icecream import ic
from gpaw.atom.generator import Generator
from gpaw.atom.configurations import parameters
from gpaw.utilities import divrl
from tgmbasis.utilities import get_basis_phit

generator = Generator("C", scalarrel=True,
                      xcname="PBE", txt=None,
                      nofiles=True)
generator.N *= 4
setup = generator.run(write_xml=False,
                      name=None,
                      **parameters[generator.symbol])
g = generator

amplitude = 12
rcut = 5.58
ri = 5.58 * 0.60

d = 1/64
eq_r = np.arange(0, rcut + d, d)
ng = len(eq_r)


ae_r = g.r
ae_dr = g.dr 

orbital_label = "2s"
n = int(orbital_label[0])
l = basis_letters.index(orbital_label[1])

for j in range(len(g.n_j)):
    if g.n_j[j] == n and g.l_j[j] == l:
        break
else:
    raise Exception(f"{orbital_label} not found in generator!")

vconf = g.get_confinement_potential(amplitude, ri, rcut)
u, e = g.solve_confined(j, rcut, vconf)
phit_g = smoothify(g, u, l)

phit_g = divrl(phit_g, 1, ae_r)

gcut = min(int(1 + rcut / d), ng - 1)
ng = gcut + 1

spline = g.rgd.spline(phit_g, ae_r[g.rgd.floor(rcut)], l, points=100)
phit_g = np.array([spline(r) * r**l for r in eq_r[:ng]])

phit_g[-1] = 0.

phit2, r2 = get_basis_phit("C", "dzdp", "/mnt/public/tgmaxson/tgm-basis/C.dzdp.basis", 0)

fig, ax = plt.subplots()

plot_phit(ax, eq_r, phit_g, label="Mine")
plot_phit(ax, r2, phit2, label="GPAW")
ax.legend()
#plt.show()