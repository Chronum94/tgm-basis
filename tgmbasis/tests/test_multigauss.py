from tgmbasis.utilities import plot_phit
from tgmbasis.phit import (phit_confined, 
                           phit_polarization_quasigauss, 
                           phit_split_valence,
                           phit_polarization_quasigauss_contract)
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gmean
from gpaw.basis_data import BasisFunction, Basis
from gpaw.atom.radialgd import EquidistantRadialGridDescriptor

r = np.arange(0, 9.5, 1/64)

phit_fpol = phit_polarization_quasigauss_contract((1.186, 1.931), (1, 0.5), (0, 0), 6.45, 3, r)
phit_fpol_dz = phit_split_valence(phit_fpol, r, 3.75, 3)
phit_fpol_tz = phit_split_valence(phit_fpol, r, 2.25, 3)

bfs = []
bfs.append(BasisFunction(None, 3, r[len(phit_fpol) - 1], phit_fpol, "f-pol multi-gauss 1"))
bfs.append(BasisFunction(None, 3, r[len(phit_fpol_dz) - 1], phit_fpol_dz, "f-pol-dz"))
bfs.append(BasisFunction(None, 3, r[len(phit_fpol_tz) - 1], phit_fpol_tz, "f-pol-tz"))
basis = Basis("Ag", "f-pols", False, EquidistantRadialGridDescriptor(1/64, len(r)))
basis.bf_j = bfs
basis.write_xml()

fig, ax = plt.subplots()
r_factor = True
plot_phit(ax, r, phit_fpol, r_factor=r_factor, label="f-pol multi-gauss 1", linestyle="-")
plot_phit(ax, r, phit_fpol_dz, r_factor=r_factor, label="f-pol-dz", linestyle="-")
plot_phit(ax, r, phit_fpol_tz, r_factor=r_factor, label="f-pol-tz", linestyle="-")
ax.legend()
plt.show()