from gpaw.atom.configurations import parameters
from gpaw.atom.generator import Generator
from tgmbasis.basis import make_soft_confined, make_QG_polarization, write_basis, collect_basis_functions

generator = Generator("Ag", scalarrel=True, xcname="PBE", txt=None, nofiles=True)
generator.N *= 8
generator.run(write_xml=True, name=None, **parameters[generator.symbol])

basis_functions, description = collect_basis_functions(
 make_soft_confined(generator, "5s", 9.27, 9.27*0.6, 12.0, []),
 make_soft_confined(generator, "4d", 5.45, 5.45*0.6, 12.0, []),
)

write_basis("Ag", basis_functions, description, "test")
