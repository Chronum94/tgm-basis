from gpaw.basis_data import Basis
from tgmbasis.phit import phit_split_valence
from tgmbasis.utilities import get_basis_phit


def test_can_get_C_dzp_phit():
    # If this test fails, GPAW's setup path is wrong or you don't have a
    # C dzp basis that can be used to test with.
    c_dzp = Basis("C", "dzdp", 2)
    assert c_dzp.symbol == "C"

    phit, r = get_basis_phit("C", "dzdp", "/mnt/public/tgmaxson/tgm-basis/C.dzdp.basis", 2)
    assert phit is not None


def test_can_split_valence():
    # Lets see if we can make a split valence basis function
    phit, r = get_basis_phit("C", "dzdp", "/mnt/public/tgmaxson/tgm-basis/C.dzdp.basis", 2)
    r_max = r[-1]

    phit_dz = phit_split_valence(phit, r, r[int(len(r)*0.8)])
    assert phit_dz.min() == 0
    assert phit_dz.sum() < phit.sum()


def test_split_valence_ends_at_0():
    # Lets see if the boundary conditions are correct. r=0 must be but
    # it's possible someone screws this up someday
    phit, r = get_basis_phit("C", "dzdp", "/mnt/public/tgmaxson/tgm-basis/C.dzdp.basis", 2)
    r_max = r[-1]

    phit_dz = phit_split_valence(phit, r, r[int(len(r)*0.8)])
    phit_tz = phit_split_valence(phit, r, r[int(len(r)*0.6)])
    phit_qz = phit_split_valence(phit, r, r[int(len(r)*0.5)])
    phit_5z = phit_split_valence(phit, r, r[int(len(r)*0.4)])
    phit_6z = phit_split_valence(phit, r, r[int(len(r)*0.3)])
    phit_7z = phit_split_valence(phit, r, r[int(len(r)*0.2)])

    assert phit_dz[-1] == 0
    assert phit_tz[-1] == 0
    assert phit_qz[-1] == 0
    assert phit_5z[-1] == 0
    assert phit_6z[-1] == 0
    assert phit_7z[-1] == 0


def test_multi_split_same():
    # This tests if sz -> tz and sz -> dz -> tz are equivalent
    # by a numerical test.  These should be equivalent!
    phit, r = get_basis_phit("C", "dzdp", "/mnt/public/tgmaxson/tgm-basis/C.dzdp.basis", 2)
    r_max = r[-1]

    phit_dz = phit_split_valence(phit, r, r[int(len(r)*0.8)])
    phit_tz1 = phit_split_valence(phit, r, r[int(len(r)*0.5)])
    phit_tz2 = phit_split_valence(phit_dz, r, r[int(len(r)*0.5)])

    assert phit_dz[-1] == 0
    assert phit_tz1[-1] == 0
    assert phit_tz2[-1] == 0

    tz_diff = (phit_tz1 - phit_tz2) * r[:len(phit_tz2)]

    assert abs(tz_diff).max() < 1e-12