from gpaw.atom.basis import QuasiGaussian
import numpy as np
import matplotlib.pyplot as plt
from tgmbasis.utilities import plot_phit
from tgmbasis.utilities import get_basis_phit
from gpaw.utilities import divrl
# EQUIDISTANT GRID
rchar = 1.4296875
rcut = 6.867187500000098
l = 2
d = 1/64
r = np.arange(0, rcut + d, d)

psi_pol = QuasiGaussian(1.0 / rchar**2, rcut)(r) * r**(l)
norm = (d * psi_pol * psi_pol).sum() ** .5
psi_pol /= norm

#psi_pol = divrl(psi_pol, 1, r) 

fig, ax = plt.subplots()

phit2, r2 = get_basis_phit("C", "dzdp", "/mnt/public/tgmaxson/tgm-basis/C.dzdp.basis", 4)

plot_phit(ax, r, psi_pol, r_factor=False, label="Mine")
plot_phit(ax, r2, phit2, r_factor=False, label="GPAW Basis", linestyle="--")

ax.set_xlim(0, rcut)

ax.legend()

#plt.show()
