from tgmbasis.confinement import SoftConfinementPotential
from tgmbasis.utilities import plot_phit
from tgmbasis.phit import phit_confined, phit_polarization_quasigauss, phit_split_valence
import numpy as np
import matplotlib.pyplot as plt
from gpaw.atom.generator import Generator
from gpaw.atom.configurations import parameters

r = np.arange(0, 9.5, 1/64)

generator = Generator("Ag", scalarrel=True, xcname="PBE", txt=None, nofiles=True)
generator.N *= 4
generator.run(write_xml=False, name=None, **parameters[generator.symbol])

confinement_5s = SoftConfinementPotential(8.28, 8.28 * 0.6, 12)
confinement_5p = SoftConfinementPotential(7.18, 7.18 * 0.6, 12)
confinement_4d = SoftConfinementPotential(5.45, 5.45 * 0.6, 12)

bf_5s = phit_confined(generator, "5s", confinement_5s, r)
bf_5p = phit_confined(generator, "4p", confinement_5p, r)
bf_4d = phit_confined(generator, "4d", confinement_4d, r)

bf_5s_sz2 = phit_split_valence(bf_5s, r, 5.51, 0)
bf_5p_sz2 = phit_split_valence(bf_5p, r, 4.51, 1)
bf_4d_sz2 = phit_split_valence(bf_5p, r, 3.26, 2)

bf_ppol = phit_polarization_quasigauss(8.28/4.5, 8.28, 1, r)
bf_fpol = phit_polarization_quasigauss(1.148, 5.45, 3, r)
bf_fpol_sz2 = phit_split_valence(bf_fpol, r, 2.07, 3)
bf_gpol = phit_polarization_quasigauss(1.148, 5.45, 4, r)

print(bf_gpol[-10:])


fig, ax = plt.subplots()
plot_phit(ax, r, bf_5s, label="5s confined")
plot_phit(ax, r, bf_5p, label="5p confined")
plot_phit(ax, r, bf_4d, label="4d confined")
plot_phit(ax, r, bf_5s_sz2, label="5s-dz")
plot_phit(ax, r, bf_5p_sz2, label="5p-dz")
plot_phit(ax, r, bf_4d_sz2, label="4d-dz")
plot_phit(ax, r, bf_ppol, label="p-pol gauss")
plot_phit(ax, r, bf_fpol, label="f-pol gauss")
plot_phit(ax, r, bf_fpol_sz2, label="f-pol-dz")
plot_phit(ax, r, bf_gpol, label="g-pol gauss")
ax.legend()
plt.show()