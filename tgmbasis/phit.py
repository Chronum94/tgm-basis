import numpy.typing as npt
from tgmbasis.utilities import derivative
import numpy as np
from gpaw.atom.basis import QuasiGaussian
from gpaw.atom.generator import Generator
from gpaw.atom.configurations import parameters
from tgmbasis.utilities import basis_letters, smoothify, resolve_grid
from tgmbasis.confinement import ConfinementPotential


def phit_confined(generator, orbital_label, confinement: ConfinementPotential, r=None):
    if isinstance(generator, str):
        generator = Generator(generator, scalarrel=True,
                              xcname="PBE", txt=None,
                              nofiles=True)
        generator.N *= 4
        generator.run(write_xml=False, name=None, **parameters[generator.symbol])
    g = generator

    rcut = confinement.r_cutoff

    if r is None:
        d = 1/64
        r = np.arange(0, rcut + d, d)
    else:
        d = r[1] - r[0]
    ng = len(r)


    ae_r = g.r
    
    n = int(orbital_label[0])
    l = basis_letters.index(orbital_label[1])

    for j in range(len(g.n_j)):
        if g.n_j[j] == n and g.l_j[j] == l:
            break
    else:
        raise Exception(f"{orbital_label} not found in generator!")
    
    confinement_potential = confinement.construct_potential(g.r)
    u, e = g.solve_confined(j, rcut, confinement_potential)
    phit_g = smoothify(g, u, l)

    spline = g.rgd.spline(phit_g, ae_r[g.rgd.floor(rcut)], l, points=1000)
    phit_g = np.array([spline(ri) for ri in r[:ng]])

    return phit_g


def phit_polarization_quasigauss(rchar: float, rcut: float, l=int, r: npt.NDArray = None):
    if r is None:
        d = 1/64
        r = np.arange(0, rcut + d, d)
    else:
        d = r[1] - r[0]

    psi_pol = QuasiGaussian(1.0 / rchar**2, rcut)(r) * r**(l)
    norm = (d * psi_pol * psi_pol).sum() ** .5
    psi_pol /= norm

    return psi_pol


def phit_polarization_quasigauss_contract(rchars: list[float], coeffs: list[float], shifts: list[float], rcut: float, l=int, r: npt.NDArray = None, operation=np.sum):
    if coeffs is None:
        coeffs = [1] * len(rchars)

    if shifts is None:
        shifts = [0] * len(rchars)

    if (len(rchars) != len(coeffs)) or (len(rchars) != len(shifts)):
        raise ValueError(f"rchars, coeffs, and shifts must be the same length")

    if r is None:
        d = 1/64
        r = np.arange(0, rcut + d, d)
    else:
        d = r[1] - r[0]

    psi_pols = []

    for coeff, rchar, shift in zip(coeffs, rchars, shifts):
        psi_pol_i = QuasiGaussian(1.0 / rchar**2, rcut - shift)(r) * r**(l)
        norm = (d * psi_pol_i * psi_pol_i).sum() ** .5
        psi_pol_i /= norm
        psi_pol_i *= coeff

        if shift != 0:
            r_i = np.min([i for i in range(len(r)) if r[i] >= shift])
            psi_pol_i[r_i:] = psi_pol_i[:-r_i]
            psi_pol_i[:r_i] *= 0

        psi_pols.append(psi_pol_i)

    psi_pol = operation(psi_pols, axis=0)
    norm = (d * psi_pol * psi_pol).sum() ** .5
    psi_pol /= norm

    return psi_pol


def phit_split_valence_old(phit: npt.NDArray, r: npt.NDArray, 
                       r_sv: int, dphit_dr: npt.NDArray=None) -> npt.NDArray:
    """
    There are errors in this function!

    Generate a split valence basis function given phit of an existing
    basis function via some other means.

    Following method from the following slides...
    https://siesta-project.org/siesta/events/SIESTA_School-2021/Basis_sets-2.pdf

    Args:
        phit (npt.NDArray): The basis function to use as a parent
        r (npt.NDArray): radial gridpoints for phit
        r_sv_i (int): radial gridpoint index for cutoff
        dphit_dr (npt.NDArray, optional): Precomputed phit derivative 

    Returns:
        npt.NDArray: New phitg trimmed such that the last gridpoint is 0
    """
    # Make copies, we don't care about memory, just make sure we don't
    # break anything stupidly.
    phit = phit.copy()
    r = r.copy()[:len(phit)]
    r_sv_i = np.min([i for i in range(len(r)) if r[i] >= r_sv])

    # Maybe we already have this and don't need to calculate again
    if dphit_dr is None:
        dphit_dr = derivative(phit[:r_sv_i + 1], r[:r_sv_i + 1])

    # Fitting parabola of form ax^2 + c
    # Note: bx term is 0 due to slope at r=0
    a = dphit_dr[r_sv_i] / (2 * r[r_sv_i])
    c = phit[r_sv_i] - a * (r[r_sv_i] ** 2)

    # Allocate enough space to hold new basis
    # Need 1 extra value at end that should be 0
    phit_sv = np.zeros(r_sv_i + 1) 

    # Here we don't go to the end of the array since we want
    # to ensure the last grid point is 0, even if the numerical
    # derivatives were not perfect
    phit_sv[:r_sv_i] = r[:r_sv_i] * (a * (r[:r_sv_i]**2) + c)
    #phit_sv[:r_sv_i] = phit[:r_sv_i] - phit_sv[:r_sv_i]

    return phit_sv


def phit_split_valence(phit: npt.NDArray, r: npt.NDArray, r_sv: int, l:int) -> npt.NDArray:
    psi_g = phit.copy()
    r_g = r.copy()[:len(phit)]
    gcut = np.min([i for i in range(len(r)) if r[i] >= r_sv])
    
    r1 = r_g[gcut]  # ensure that rcut is moved to a grid point
    r2 = r_g[gcut + 1]
    y1 = psi_g[gcut] / r_g[gcut]
    y2 = psi_g[gcut + 1] / r_g[gcut + 1]
    b = - (y2 / r2**l - y1 / r1**l) / (r2**2 - r1**2)
    a = (y1 / r1**l + b * r1**2)
    psi_g2 = r_g**(l + 1) * (a - b * r_g**2)
    psi_g2[gcut:] = psi_g[gcut:]

    phit_g = psi_g - psi_g2
    phit_g[-1] = 0.
    return phit_g


def phit_reduce(phit: npt.NDArray, min_val: float=1e-12) -> npt.NDArray:
    for index in range(len(phit) - 1, 0, -1):
        if phit[index] > min_val:
            break

    reduced_phit = phit[:index + 1]
    reduced_phit[-1] = 0

    return reduced_phit