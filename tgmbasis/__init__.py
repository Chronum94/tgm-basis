# Required information
# Confined Basis Function
# - r_cutoff(orbital): cutoff radiusi
# - r_i(orbital): confinement start radius
# - alpha(orbital): confinement strength

# Split-Valence Basis Function
# - r_m(orbital, zeta): split valence radius

# Polarization Basis Function
# - r_pol(l): cutoff radius for polarization
# - r_char: characteristic_length for polarization gaussian

from gpaw.atom.radialgd import AERadialGridDescriptor
from gpaw.atom.radialgd import EquidistantRadialGridDescriptor
from gpaw.atom.basis import BasisMaker as BasisMakerGPAW
from gpaw.atom.basis import QuasiGaussian
from dataclasses import dataclass
from icecream import ic

# <basis_function n="2" l="0" rc="5.3125" type="2s-sz confined orbital" grid="grid1">

class GenericBasis(object):
    child_basis: 'GenericBasis' = None


@dataclass 
class OccupiedBasis(GenericBasis):
    orbital_label: str
    r_cutoff: float
    r_confine_start: float
    alpha: float = 12
    split_cutoffs: tuple[float] = tuple()

    def __repr__(self):
        return f"OccupiedBasis(orbital_label='{self.orbital_label}', " +\
               f"r_cutoff={self.r_cutoff}, " +\
               f"r_confine_start={self.r_cutoff}, " +\
               f"alpha={self.alpha}," +\
               f"split_cutoffs={self.split_cutoffs})"
    
@dataclass
class GaussianPolarizationBasis(GenericBasis):
    l: str
    r_cutoff: float
    char_length: float
    split_cutoffs: tuple[float] = tuple()

    def __repr__(self):
        return f"GaussianPolarizationBasis(l={self.l}, " +\
               f"r_cutoff={self.r_cutoff}, " +\
               f"char_length={self.char_length}, " +\
               f"split_cutoffs={self.split_cutoffs})"


class BasisMaker(object):
    def __init__(self, generator, name=None, xc="PBE"):
        self.generator = generator
        self.rgd = AERadialGridDescriptor(generator.beta / generator.N,
                                          1.0 / generator.N, generator.N,
                                          default_spline_points=100)

        # Lets grab gpaw default functions
        self.smoothify = BasisMakerGPAW.smoothify

    def generate(self):
        pass

    def plot(self):
        raise NotImplementedError()