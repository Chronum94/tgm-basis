from dataclasses import dataclass
import numpy as np

from tgmbasis.utilities import resolve_grid


@dataclass 
class ConfinementPotential(object):
    r_cutoff: float
    
    def construct_potential(self, r):
        """
        Construct potential on the grid r_grid
        """
        raise NotImplementedError("Base class cannot be used to construct potential")


@dataclass
class HardConfinementPotential(ConfinementPotential):
    def construct_potential(self, r):
        return None


@dataclass
class SoftConfinementPotential(ConfinementPotential):
    r_interact: float
    amplitude: float

    def construct_potential(self, r):
        """Create a smooth confinement potential.

        Returns a (potential) function which is zero inside the radius ri
        and goes to infinity smoothly at rc, after which point it is nan.
        The potential is given by::

                   alpha         /   rc - ri \
          V(r) = --------   exp ( - --------- )   for   ri < r < rc
                  rc - r         \    r - ri /

        """
        i_ri = None
        i_rc = None

        ri = self.r_interact
        rc = self.r_cutoff

        for index, val in enumerate(r):
            if val >= self.r_interact and i_ri is None:
                i_ri = index
            if val >= self.r_cutoff and i_rc is None:
                i_rc = index
                break
        else:
            raise ValueError("Radial cutoffs are larger than maximum radius in potential!")

        if r[i_rc] == rc: # Grid aligns with rc perfectly
            rc -= 1

        potential = np.zeros(np.shape(r))
        r = r[i_ri + 1:i_rc + 1]
        exponent = - (rc - ri) / (r - ri)
        denom = rc - r
        value = np.exp(exponent) / denom
        potential[i_ri + 1:i_rc + 1] = value
        potential[i_rc + 1:] = np.inf

        return self.amplitude * potential